﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaFACC_Thech
{
    
    //Aqui heredo la interfas IProcesosDeFacturacion y IDescuento
    class Calculos_A_pagar_Premiun : IProcesosDeFacturacion,IDescuento
    {
        public decimal Subtotal { get; set; }
        public decimal Descuento { get; set; }
        public decimal Total { get; set; }
        public IListaProCliente myFactura;
        //en el constructor se crea el parametro tipo FacturaCarrito, para resibir datos tipo carrito como  argumento 
        //esto se almacena en el atributo IListaProClient
        public Calculos_A_pagar_Premiun(FacturaCarrito facturaCarrito)
        {
            myFactura = facturaCarrito;
        }

        public decimal CalcularSubtotal()
        {
            decimal conta = 0;
            IEnumerable<DetalleFactura> ListaClientesSin = myFactura.ListaClientes();
            foreach (var item in ListaClientesSin)
            {
                conta = conta + (item.Cantidad * item.ProductoCarrito.Precio);
            }
            this.Subtotal = conta;
            return Subtotal;
        }
        
        public decimal CalcularDescuento()
        {
            this.Descuento=this.Subtotal*0.10M;
            return Descuento;
        }
               
        public decimal CalcularTotal()
        {
            this.Total = Subtotal - Descuento;
            return Total;
        }

        public void ImprimirCalculo()
        {
            Console.WriteLine("Subtotal...:" + CalcularSubtotal());
            Console.WriteLine("Descuento...:" + CalcularDescuento());
            Console.WriteLine("Su total es..:" + CalcularTotal()); ;
        }
    }
}
