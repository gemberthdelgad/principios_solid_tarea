﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaFACC_Thech
{
    //Creo la interfas IProcesosDeFacturacion que contiene las funsiones para utilisarlas en las cleses que se dedican hacer los calculos de las compras para 
    //utilizar de mejor manera el poliformismo
    interface IProcesosDeFacturacion
    {
        public decimal CalcularSubtotal();
        
       public decimal CalcularTotal();
        public void ImprimirCalculo();
    }
}
