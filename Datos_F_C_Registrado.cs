﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaFACC_Thech
{
    class Datos_F_C_Registrado
    {
        // Creo tres clases de datos facturas , que se encargan exclusiva mente de llenar los datos del cliente 
        public ClienteConRegistro Dato2 { get; set; }
        public void LlenarDatos()
        {
           
            
            ClienteConRegistro c2 =new ClienteConRegistro();
            Console.WriteLine("Escriba su Nombre"); c2.Nombres = Console.ReadLine();
            Console.WriteLine("Escriba su Apellido"); c2.Apellido = Console.ReadLine();
            Console.WriteLine("Escriba su Numero de cedula"); c2.Cedula = Console.ReadLine();
            Console.WriteLine("Escriba su Correo"); c2.Email = Console.ReadLine();
            Console.WriteLine("Escriba su Contraseña"); c2.Contraseña = Console.ReadLine();
            Console.WriteLine("Ciudad de residencia"); c2.Ciudad = Console.ReadLine();
            Console.WriteLine("Escriba su Fecha de Registro"); c2.FechaInicioRegistro = DateTime.Parse(Console.ReadLine());
            Console.WriteLine("Escriba fin de registro"); c2.FechaFinalisacion = DateTime.Parse(Console.ReadLine());
            this.Dato2 = c2;
        }
        //Funsion que se encarga de sabeer los años de resgistro
        private decimal FechaRegistro()
        {
            TimeSpan timeSpan = DateTime.Today - Dato2.FechaInicioRegistro;
            int dias = timeSpan.Days;
            int fecha = dias / 365;
            return fecha;
        }
        //private decimal FechaFinRegistro()
        //{
        //    TimeSpan timeSpan = DateTime.Today - Dato2.FechaFinalisacion;
        //    int dias = timeSpan.Days;
        //    int fecha = dias / 365;
        //    return fecha;
        //}
        //metodo que imprime la fecha de registro
        public void ImprimirFechas()
        {
            Console.WriteLine("Se registro hace ...:" + FechaRegistro() + "años");
        }

    }
}
