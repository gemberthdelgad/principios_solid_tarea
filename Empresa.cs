﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaFACC_Thech
{
    public class Empresa
    {
        public string RazonSocial { get; set; }
        public string DireccionEmpresa { get; set; }
    }
}
