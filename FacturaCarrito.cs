﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaFACC_Thech
{
    //en esta clase contiene la lista de los productos, aqui heredo la interfas IListaProCliente donde guardare lo que contenga la lista  detalleFactura

    public class  FacturaCarrito:IListaProCliente
    {

        public FacturaCarrito()
        {
            this.Detalle = new List<DetalleFactura>();
        }
       
        private List<DetalleFactura> detalle;
        
        public List<DetalleFactura> Detalle
        {
            get { return detalle; }
            set { detalle = value; }
        }
        public void ListaDeFactura()
        {
            Console.WriteLine("-------------------------Productos comprados------------------------");
            Console.WriteLine();
            Console.WriteLine("Codigo-------Productos ------------Cantidad----------Precio------------");
            Console.WriteLine();
            foreach (var item in Detalle)
            {
                Console.WriteLine("\t{0}\t\t{1}\t\t{2}\t\t{3}", item.ProductoCarrito.Codigo, item.ProductoCarrito.Descripcion, item.Cantidad, item.ProductoCarrito.Precio);

            }


        }
        // En esta funcion heredada de la interfas se guarga la lista de detalle
        // va conpliendo el principio el principio de inversion de dependencia
        public IEnumerable<DetalleFactura> ListaClientes()
        {
            List<DetalleFactura> Listado = new List<DetalleFactura>();
           
            foreach (var item in this.Detalle)
            {
                Listado.Add(item);
            }
            return Listado;
        }

       


    }
}

