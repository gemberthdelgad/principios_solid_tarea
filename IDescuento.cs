﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaFACC_Thech
{
    //Creo esta interfas aparte porque es posible que en algun tipo de cliente no se utilice la funcion  Calculardescuento 
    //esto ayuda a no generar execciones al no utilisar alguna funcion en el futuro
    interface IDescuento
    {
        public decimal CalcularDescuento();
    }
}
