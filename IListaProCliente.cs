﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaFACC_Thech
{
    //creo esta interfas IListaProCliente que contiene la funcion de tipo "IEnumerable<DetalleFactura>"  para hacer menos dependiente a las clases que calculan el precio 
    interface IListaProCliente
    {
        IEnumerable<DetalleFactura> ListaClientes();
    }
}
