﻿using System;
using System.Collections.Generic;
using System.Text;

namespace   TiendaFACC_Thech
{
    class ClienteConRegistro:Cliente
    {
        public DateTime FechaInicioRegistro { get; set; }
        public DateTime FechaFinalisacion { get; set; }
    }
}
