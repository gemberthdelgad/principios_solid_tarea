﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaFACC_Thech
{
    //Aqui herredo la interfas IProcesosDeFacturacion y IDescuento
    class Calculos_A_Pagar_Registrado : IProcesosDeFacturacion,IDescuento
    {
        public decimal Subtotal { get; set; }
        public decimal Descuento { get; set; }
        public decimal Total { get; set; }
        //Creamos el atributo IListaProCliente que es una interfas 
        //asi vamos construyendo esto modularmente al no depender tanto de la clase de "bajo nivel" "facturacarrito"
        public IListaProCliente myFactura;

        public Calculos_A_Pagar_Registrado(FacturaCarrito facturaCarrito)
        {
            myFactura = facturaCarrito;
        }
        public decimal CalcularSubtotal()
        {
            //grasias a esto si se presenta algun cambio en la lista de productos solo se realisarian los cambios en aquella clase "facturra Carrito" y no en ambas
            // aqui se esta utilizando el 5to principio 
            decimal conta = 0;
            IEnumerable<DetalleFactura> ListaClientesSin = myFactura.ListaClientes();
            foreach (var item in ListaClientesSin)
            {
                conta = conta + (item.Cantidad * item.ProductoCarrito.Precio);
            }
            this.Subtotal = conta;
            return Subtotal;
        }

        public decimal CalcularDescuento()
        {
            this.Descuento = this.Subtotal * 0.05M;
                    
            return Descuento;
        }

        

        public decimal CalcularTotal()
        {
            this.Total = Subtotal - Descuento;
            return Total; 
        }

        public void ImprimirCalculo()
        {
            Console.WriteLine("Subtotal...:" + CalcularSubtotal());
            Console.WriteLine("Descuento...:" + CalcularDescuento());
            Console.WriteLine("Su total es..:" + CalcularTotal()); ;
        }
    }
}
