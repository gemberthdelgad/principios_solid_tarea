﻿using System;

namespace TiendaFACC_Thech
{
    class Program
    {
        static void Main(string[] args)
        {
            //ahora el codigo es mas modular y mantenible grasias a los principios solid
          
            ListaDeProductos listaDeProductos = new ListaDeProductos();
            listaDeProductos.GeneraProductos();
            listaDeProductos.imprimirlistado();
            Cliente cliente = new Cliente();
            cliente.Apellido = "Delgado";
            cliente.Nombres = "Gemberth";        
            cliente.Cedula = "1314897324";
            cliente.Email = "gjdd@gmail";

            Empresa empresa = new Empresa();
            empresa.RazonSocial = "Tienda Facci";
            empresa.DireccionEmpresa = "Manta";

            CabeceraDeFactura cabeceracarrito = new CabeceraDeFactura();
            cabeceracarrito.ClienteCabecera = cliente;
            cabeceracarrito.EmpresaCabecera = empresa;
            

            FacturaCarrito carrito = new FacturaCarrito();
            string opcion;
            do
            {
                Console.WriteLine("Ingrese el codigo del producto");
                int codigo = int.Parse(Console.ReadLine());

                DetalleFactura detalleFactura = new DetalleFactura();
                detalleFactura.ProductoCarrito = listaDeProductos.StockProductos[codigo - 1];

                Console.WriteLine("Ingrese los productos que quiera llevar");
                int cantidadproducto = int.Parse(Console.ReadLine());

                detalleFactura.Cantidad = cantidadproducto;

               

                carrito.Detalle.Add(detalleFactura);
                
                Console.WriteLine("Escriba A para seguir agregando productus y S para salir");
                opcion = Console.ReadLine();


            } while (opcion != "S");
            int menu;
           // do
           // {

                //int menu;
            Console.WriteLine("-------------------Bienvenidos al proceso de facturación----------------------");
            Console.WriteLine("1.Cliente no afilado" +
                              " \n2.-Cliente Registrado" +
                               "\n3.-Cliente Premiun"+
                               "\n4.-Canselar la Compra");menu =int.Parse( Console.ReadLine());
            
            // por ser tres tipos de clientes uso un swich 
            switch(menu)
            { case 1:
                    //esta funsion ase una linpiesa de pantalla
                    Console.Clear();
                    //aqui envio como argumento a carrito para ser prosesado en la clase Calculos.. asi mismo con los dos casosos que siguen 
                   Calculos_a_pagar_sin_Registro factura = new Calculos_a_pagar_sin_Registro(carrito);                    
                    Datos_F_C_Sin_Registro f1 = new Datos_F_C_Sin_Registro();
                    Console.WriteLine("--------------------------Ingrese sus datos---------------------------");
                    Console.WriteLine();
                    //Se pregunta si desea agregar datos
                    Console.WriteLine("si o no");string op = Console.ReadLine();
                    if (op == "si") { f1.LlenarDatos(); } else { }
                    Console.WriteLine("-----------------------------------------------------------------------");
                    carrito.ListaDeFactura();               
                                 
               Console.WriteLine("-----------------------------------------------------------------------");                                                   
              factura.ImprimirCalculo();
            
                    break;

                case 2:
                    Console.Clear();
                    Datos_F_C_Registrado f2 = new Datos_F_C_Registrado();
                    Calculos_A_Pagar_Registrado c2 = new Calculos_A_Pagar_Registrado(carrito);
                    Console.WriteLine("--------------------------Ingrese sus datos---------------------------");
                    Console.WriteLine();
                    
                     f2.LlenarDatos(); 
                    Console.WriteLine("-----------------------------------------------------------------------");
                    carrito.ListaDeFactura();
                    Console.WriteLine("------------------------------------------------------------------------");
                    c2.ImprimirCalculo();
                    Console.WriteLine("--------------------------------------------------------------------------");
                    f2.ImprimirFechas();
                    break;
                case 3:
                    Console.Clear();
                    Datos_F_C_Premiun f3 = new Datos_F_C_Premiun();
                    Calculos_A_pagar_Premiun c3 = new Calculos_A_pagar_Premiun(carrito);
                    Console.WriteLine("--------------------------Ingrese sus datos---------------------------");
                    Console.WriteLine();
                   

                   f3.LlenarDatos();
                    Console.WriteLine("-----------------------------------------------------------------------");
                    carrito.ListaDeFactura();
                    Console.WriteLine("------------------------------------------------------------------------");
                    c3.ImprimirCalculo();
                    Console.WriteLine("------------------------------------------------------------------------");
                    f3.ImprimirFechas();
                    break;
                    case 4:
                        Console.WriteLine("Gracias por visitarnos vuelva pronto");
                        break;
                    default:
                    Console.WriteLine("Ha seleccionado una opcion no valida" +
                        "\nPor favor seleccione una opcion dentro del rango[1-4]");
                    break;
            }
           // } while (menu != 4);

        }
    }
}
