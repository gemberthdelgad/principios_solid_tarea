﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaFACC_Thech
{
   public class Cliente
    {
        public string Cedula { get; set; }
        public string Nombres { get; set; }

        public string Apellido { get; set; }

        public string Email { get; set; }

        public string Contraseña { get; set; }
        public string Ciudad { get; set; }

    }
}
