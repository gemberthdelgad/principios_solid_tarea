﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaFACC_Thech
{
    //Aqui herredo la interfas IProcesosDeFacturacion y IDescuento 
    class Calculos_a_pagar_sin_Registro :IProcesosDeFacturacion,IDescuento
        

    {
        public Calculos_a_pagar_sin_Registro( FacturaCarrito facturaCarrito )
        {
            myFactura = facturaCarrito;
           
        }
        public decimal Subtotal { get; set; }
        public decimal Descuento { get; set; }
        public decimal Total { get; set; }

      
        public IListaProCliente myFactura;
        public decimal CalcularSubtotal()
        {
             
             decimal conta = 0;
            IEnumerable<DetalleFactura> ListaClientesSin = myFactura.ListaClientes();
             foreach ( var item in ListaClientesSin)
             {
                 conta = conta + (item.Cantidad * item.ProductoCarrito.Precio);
             }
             this.Subtotal = conta;
            return Subtotal;
        }
       

        public decimal CalcularDescuento()
        {
            if (this.Subtotal >= 100)
            { this.Descuento = this.Subtotal * 0.05M; }
            else { this.Descuento = 0; }
            return Descuento;
         }
        public decimal CalcularTotal()
        {
            this.Total = Subtotal - Descuento;
            return Total;
        }
        public void ImprimirCalculo()
        {
            Console.WriteLine("Subtotal...:" + CalcularSubtotal());
            Console.WriteLine("Descuento...:" + CalcularDescuento());
            Console.WriteLine("Su total es..:" + CalcularTotal());
        }
       
    }

}
